# Log de Atividades

*Tiago de Carvalho Gallo Pereira*
*Aluno regular de mestrado do PPGI - Universidade de Brasília*

## Semana do dia 01/02/20 ao dia 07/02/20

* Trabalhei na apresentação do VISAPP

## Semana do dia 27/01/20 ao dia 31/01/20

* Criei um novo [repositório](https://gitlab.com/TiagoGallo/estudos-mestrado) com resumos/anotações de artigos estudados
* Enviei o email para o Krystian me apresentando e dizendo quando estarei no Imperial College
* Terminei os estudos do artigo [Partial Person Re-identification with Alignment and Hallucination](https://arxiv.org/abs/1807.09162)
* Estudo do artigo [Person Re-identification with Bias-controlled Adversarial Training](https://arxiv.org/abs/1904.00244)
* Comecei a preparar a apresentação do VISAPP
* Estudo do artigo [Compression of convolutional neural networks for high performance imagematching tasks on mobile devices](https://arxiv.org/abs/2001.03102)

## Semana do dia 05/09/19 ao dia 11/10/19

* Preparei e dei aula de PVC
* Estudos de processos estocásticos
* Estudos de PCA
* Alguns Testes extras para terminar o paper do VISAPP
* Pretendo terminar o paper no fds

## Semana do dia 29/09/19 ao dia 04/10/19

* Papers para o WPOS e VISAPP
* Estudos de processos estocásticos
* Estudos de PCA
* Atendi o Rafael de PVC

## Semana do dia 06/09/19 ao dia 12/09/19

* Finalizada lista de processos estocásticos
* Correção de um PD1 de PVC que tinha passado despercebido
* Estudos Processos estocásticos
* Lista Processos estocásticos
* Reunião UnBVision
* Estudos de PCA

## Semana do dia 31/08/19 ao dia 05/08/19

* Estudos de Processos estocásticos
* Finalizei correções do PD1 de PVC
* Estudos de PCA
* Finalizei a lista de PCA
* Correção do PD1 de PVC
* Estudos de Processos Estocásticos
* Estudos de PCA
* Lista de PCA

## Semana do dia 24/08/19 ao dia 30/08/19

* Reunião UnB Vision
* Adaptação do enunciado do PD2 de PVC
* Revisão do PD2 de Visão Computacional e proposta de uma medição de mão
* Preparação de um código Python para os alunos terem um ponto de partida no PD2
* Estudos projeto e complexidade de algoritmos
* Estudo processos estocásticos
* Lista de projeto e complexidade de algoritmos
* Lista de processos estocásticos

## Semana do dia 16/08 ao dia 23/08

* Apenas assisti as aulas e estudei para as matérias de processos estocásticos e PCA

## Semana do dia 29/06 ao dia 05/07

* Preparando a apresentação para a defesa do TG2
* Apresentação de uma cycleGAN sketch2face para a matéria do professor Romariz ([link da apresentação](https://docs.google.com/presentation/d/10098YYDqcl2pWqyvs5nZIyVMJVodZQ3hakCmPXK4egI/edit#slide=id.g5ca2d7f931_0_77), [código](https://gitlab.com/TiagoGallo/sistemas-inteligentes/tree/master/Trabalho%20Final))
* Assisti o vídeo da apresentação do taskonomy na CVPR 2018 ([link](https://www.youtube.com/watch?v=9mdCWMVAMLg)) 

## Semanas do dia 15/06 ao dia 21/06 e do dia 22/06 ao dia 28/06

* Finalizando experimentos e relatório do TG2

## Semana do dia 08/06 ao dia 14/06

* Escrevendo o relatório do TG2 (foco na metodologia)
* Alguns treinamento de cycleGAN e de redes utlizando a cycleGAN como data augmentation

## Semana do dia 01/06 ao dia 07/06

* Terminei de escrever a fundamentação teórica 
* Complementei a parte dos objetivos na introdução teórica
* Escrevi parte do capítulo de metodologia e reestruturei o capítulo de resultados (algumas seções foram para o capítulo de metodologia)

## Semana do dia 25/05 ao dia 31/05

* Escrita dos fundamentos teóricos no relatório do TG2 (ainda falta escrever um pouco sobre domain adaptation e learning rate cíclica/superconvergência, mas já tenho um pouco disso escrito nos meus rascunhos)
* Criado capítulo de metodologia e uma primeira ideia de estruturação para ele
* Adicionado um resumo dos resultados obtidos até agora e uma análise de quais resultados precisam ser "revistos" (retreinados)
* Primeiro treinamento completo de uma CycleGAN transformando imagens do Market1501 para o CUHK03
* Utilização da CycleGAN acima como data augmentation para o treinamento de uma rede na Market1501
* Encontrado possível bug no função de custo

## Semana do dia 18/05 ao dia 24/05

* Mais alguns testes/experiências com CycleGAN
* Estudo do artigo "Unpaired Image-to-Image Translation using Cycle-Consistent Adversarial Networks" (links para [website](https://junyanz.github.io/CycleGAN/) e [Two Minutes Paper video](https://www.youtube.com/watch?v=D4C1dB9UheQ))
* Convite ao professor Romariz para a banca

## Semana do dia 11/05 ao dia 17/05

* Criado esse repositório para acompanhamento das atividades 
* Arrumado o script para fazer transfer learning completo entre os modelos do dataset
* Estudo sobre domain adaptation -> revisão de alguns pontos do artigo da Gabriela Csurka, principalmente para utilizar a mesma notação (escrevi algumas coisas no capitulo de rascunhos)
* Primeiros treinamentos de cycleGAN
* Estudos sobre GAN -> lido o artigo "Generative Adversarial Nets" do Goodfellow (pontos genéricos sobre GAN que devem entrar no relatório do TG foram escritos no cap de rascunhos)