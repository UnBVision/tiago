# Tiago de Carvalho Gallo Pereira
# 19/0144874

## Repositório referente ao texto e apresentação do TG:

* [Raiz do repositório](https://gitlab.com/TiagoGallo/TG_Person_Re-Identification)
* [Apresetação](https://gitlab.com/TiagoGallo/TG_Person_Re-Identification/tree/master/Apresentacao)
* [Rascunhos sobre alguns artigos lidos](https://gitlab.com/TiagoGallo/TG_Person_Re-Identification/blob/master/docs/cap_Rascunhos.tex)
* [Arquivos fonte LaTex](https://gitlab.com/TiagoGallo/TG_Person_Re-Identification/tree/master/docs)

Dica: Na pasta com os arquivos fonte LaTex tem um bash script para compilar o documento com pdflatex e abrir o pdf do relatório no evince, 
esse script foi preparado para rodar em Ubuntu, mas deve funcionar em outras distribuições do Linux. 
Para rodar basta digitar o seguinte comando no terminal:

```bash
./compile.sh
```

## Links importantes

* [Paper enviado para o WPOS](https://www.overleaf.com/read/wyjkhndrtxzh)
* [Paper para o VISAPP](https://www.overleaf.com/read/csrvhqprdrdk)
* [Repositório com resumos/anotações de artigos lidos](https://gitlab.com/TiagoGallo/estudos-mestrado)